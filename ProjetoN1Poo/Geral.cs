﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoN1Poo {
    public class Geral {
        public enum Curso { Ensino_Fundamental, Ensino_Medio, Administração, Engenharia_de_Controle_e_Automação, Engenharia_de_Alimentos, Engenharia_da_Computação }
        public static class Alunos {
            public static List<Aluno> all = new List<Aluno>();

            public static bool exists_RA(string x) {
                return all.Exists(a => a.RA == x);
            }

            public static bool exists_A(String x) {
                return all.Exists(a => a.Name == x);
            }

            public static int count() {
                return Alunos.all.Count;
            }

            public static void add(Aluno x) {
                if (all.Find(a => a.Name == x.Name) == null)
                    Alunos.all.Add(x);
                else
                    throw new Exception("Já existe um aluno com esse nome.");
            }
        }

    }
}
