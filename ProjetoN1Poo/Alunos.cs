﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoN1Poo {
    public partial class ListagemAlunos : Form {
        public ListagemAlunos() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        private void Alunos_Load(object sender, EventArgs e) {
            for(var x = 0; x < Geral.Alunos.all.Count; x++) {
                var a = Geral.Alunos.all[x];
                if (x != 0)
                    richTextBox1.Text += "\n\n\n" + a.list_grades();
                else
                    richTextBox1.Text += a.list_grades();
            }
        }
    }
}
