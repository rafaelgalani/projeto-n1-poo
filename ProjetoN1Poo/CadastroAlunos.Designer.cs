﻿namespace ProjetoN1Poo {
    partial class CadastroAlunos {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.nome_c = new System.Windows.Forms.TextBox();
            this.data_c = new System.Windows.Forms.DateTimePicker();
            this.curso_c = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.periodo_c = new System.Windows.Forms.ComboBox();
            this.prox_c = new System.Windows.Forms.Button();
            this.freq_c = new System.Windows.Forms.ComboBox();
            this.freq_v = new System.Windows.Forms.NumericUpDown();
            this.nota_c1 = new System.Windows.Forms.ComboBox();
            this.nota_c2 = new System.Windows.Forms.ComboBox();
            this.nota_v = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.freq_q = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.nota_q = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.alunos_c = new System.Windows.Forms.Label();
            this.add_c = new System.Windows.Forms.Button();
            this.end_c = new System.Windows.Forms.Button();
            this.erro_c = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.freq_v)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nota_v)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erro_c)).BeginInit();
            this.SuspendLayout();
            // 
            // nome_c
            // 
            this.nome_c.Location = new System.Drawing.Point(126, 35);
            this.nome_c.Name = "nome_c";
            this.nome_c.Size = new System.Drawing.Size(100, 20);
            this.nome_c.TabIndex = 2;
            // 
            // data_c
            // 
            this.data_c.Location = new System.Drawing.Point(126, 61);
            this.data_c.MaxDate = new System.DateTime(2016, 5, 1, 0, 0, 0, 0);
            this.data_c.Name = "data_c";
            this.data_c.Size = new System.Drawing.Size(100, 20);
            this.data_c.TabIndex = 4;
            this.data_c.Value = new System.DateTime(2016, 5, 1, 0, 0, 0, 0);
            // 
            // curso_c
            // 
            this.curso_c.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.curso_c.FormattingEnabled = true;
            this.curso_c.Location = new System.Drawing.Point(126, 86);
            this.curso_c.Name = "curso_c";
            this.curso_c.Size = new System.Drawing.Size(100, 21);
            this.curso_c.TabIndex = 5;
            this.curso_c.SelectedIndexChanged += new System.EventHandler(this.curso_c_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nome:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Data de ingresso:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Período:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(81, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Curso:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Frequência:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Notas:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.periodo_c);
            this.groupBox1.Controls.Add(this.prox_c);
            this.groupBox1.Controls.Add(this.curso_c);
            this.groupBox1.Controls.Add(this.nome_c);
            this.groupBox1.Controls.Add(this.data_c);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(270, 218);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pessoal";
            // 
            // periodo_c
            // 
            this.periodo_c.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.periodo_c.FormattingEnabled = true;
            this.periodo_c.Location = new System.Drawing.Point(126, 112);
            this.periodo_c.Name = "periodo_c";
            this.periodo_c.Size = new System.Drawing.Size(100, 21);
            this.periodo_c.TabIndex = 6;
            // 
            // prox_c
            // 
            this.prox_c.Location = new System.Drawing.Point(19, 156);
            this.prox_c.Name = "prox_c";
            this.prox_c.Size = new System.Drawing.Size(234, 31);
            this.prox_c.TabIndex = 7;
            this.prox_c.Text = "Próximo";
            this.prox_c.UseVisualStyleBackColor = true;
            this.prox_c.Click += new System.EventHandler(this.prox_c_Click);
            // 
            // freq_c
            // 
            this.freq_c.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.freq_c.FormattingEnabled = true;
            this.freq_c.Location = new System.Drawing.Point(70, 1);
            this.freq_c.Name = "freq_c";
            this.freq_c.Size = new System.Drawing.Size(175, 21);
            this.freq_c.TabIndex = 0;
            // 
            // freq_v
            // 
            this.freq_v.DecimalPlaces = 1;
            this.freq_v.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.freq_v.Location = new System.Drawing.Point(251, 2);
            this.freq_v.Name = "freq_v";
            this.freq_v.Size = new System.Drawing.Size(66, 20);
            this.freq_v.TabIndex = 2;
            // 
            // nota_c1
            // 
            this.nota_c1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.nota_c1.FormattingEnabled = true;
            this.nota_c1.Location = new System.Drawing.Point(70, 16);
            this.nota_c1.Name = "nota_c1";
            this.nota_c1.Size = new System.Drawing.Size(92, 21);
            this.nota_c1.TabIndex = 2;
            // 
            // nota_c2
            // 
            this.nota_c2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.nota_c2.FormattingEnabled = true;
            this.nota_c2.Location = new System.Drawing.Point(168, 16);
            this.nota_c2.Name = "nota_c2";
            this.nota_c2.Size = new System.Drawing.Size(77, 21);
            this.nota_c2.TabIndex = 3;
            // 
            // nota_v
            // 
            this.nota_v.DecimalPlaces = 1;
            this.nota_v.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.nota_v.Location = new System.Drawing.Point(251, 17);
            this.nota_v.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nota_v.Name = "nota_v";
            this.nota_v.Size = new System.Drawing.Size(66, 20);
            this.nota_v.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Location = new System.Drawing.Point(302, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(335, 211);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Acadêmico";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.freq_v);
            this.groupBox3.Controls.Add(this.freq_q);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.freq_c);
            this.groupBox3.Location = new System.Drawing.Point(7, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(326, 75);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            // 
            // freq_q
            // 
            this.freq_q.Location = new System.Drawing.Point(3, 32);
            this.freq_q.Name = "freq_q";
            this.freq_q.Size = new System.Drawing.Size(314, 31);
            this.freq_q.TabIndex = 1;
            this.freq_q.Text = "Cadastrar";
            this.freq_q.UseVisualStyleBackColor = true;
            this.freq_q.Click += new System.EventHandler(this.freq_q_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.nota_q);
            this.groupBox4.Controls.Add(this.nota_v);
            this.groupBox4.Controls.Add(this.nota_c2);
            this.groupBox4.Controls.Add(this.nota_c1);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(7, 102);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(327, 98);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            // 
            // nota_q
            // 
            this.nota_q.Location = new System.Drawing.Point(3, 47);
            this.nota_q.Name = "nota_q";
            this.nota_q.Size = new System.Drawing.Size(314, 31);
            this.nota_q.TabIndex = 4;
            this.nota_q.Text = "Cadastrar";
            this.nota_q.UseVisualStyleBackColor = true;
            this.nota_q.Click += new System.EventHandler(this.nota_c2_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(473, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Total de alunos cadastrados:";
            // 
            // alunos_c
            // 
            this.alunos_c.AutoSize = true;
            this.alunos_c.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.alunos_c.Location = new System.Drawing.Point(616, 5);
            this.alunos_c.Name = "alunos_c";
            this.alunos_c.Size = new System.Drawing.Size(21, 13);
            this.alunos_c.TabIndex = 9;
            this.alunos_c.Text = "10";
            // 
            // add_c
            // 
            this.add_c.Enabled = false;
            this.add_c.Location = new System.Drawing.Point(12, 250);
            this.add_c.Name = "add_c";
            this.add_c.Size = new System.Drawing.Size(269, 29);
            this.add_c.TabIndex = 2;
            this.add_c.Text = "Adicionar";
            this.add_c.UseVisualStyleBackColor = true;
            this.add_c.Click += new System.EventHandler(this.add_c_Click);
            // 
            // end_c
            // 
            this.end_c.Enabled = false;
            this.end_c.Location = new System.Drawing.Point(302, 250);
            this.end_c.Name = "end_c";
            this.end_c.Size = new System.Drawing.Size(335, 29);
            this.end_c.TabIndex = 3;
            this.end_c.Text = "Finalizar";
            this.end_c.UseVisualStyleBackColor = true;
            this.end_c.Click += new System.EventHandler(this.end_c_Click);
            // 
            // erro_c
            // 
            this.erro_c.ContainerControl = this;
            // 
            // CadastroAlunos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 291);
            this.Controls.Add(this.end_c);
            this.Controls.Add(this.add_c);
            this.Controls.Add(this.alunos_c);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CadastroAlunos";
            this.Text = "Cadastro de alunos";
            this.Load += new System.EventHandler(this.CadastroAlunos_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.freq_v)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nota_v)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erro_c)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nome_c;
        private System.Windows.Forms.DateTimePicker data_c;
        private System.Windows.Forms.ComboBox curso_c;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox freq_c;
        private System.Windows.Forms.NumericUpDown freq_v;
        private System.Windows.Forms.ComboBox nota_c1;
        private System.Windows.Forms.ComboBox nota_c2;
        private System.Windows.Forms.NumericUpDown nota_v;
        private System.Windows.Forms.ComboBox periodo_c;
        private System.Windows.Forms.Button prox_c;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button freq_q;
        private System.Windows.Forms.Button nota_q;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label alunos_c;
        private System.Windows.Forms.Button add_c;
        private System.Windows.Forms.Button end_c;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ErrorProvider erro_c;
    }
}

