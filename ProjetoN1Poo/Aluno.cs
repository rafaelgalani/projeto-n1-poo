﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoN1Poo {

    public class Aluno : Geral {

        private string _name;
        private DateTime _start_date;
        private int _period;
        private Curso _course;
        private string _RA;
        private Dictionary<int, double[]> _period_grade;
        private Dictionary<int, double> _period_freq;

        private void reset_all() {
            this.Name = "";
            this._start_date = new DateTime();
            this._period = 0;
            this._course = 0;
            this.RA = "";
        }

        #region Get e set
        public Curso curso {
            get {
                return _course;
            }
            set {
                _course = value;
            }
        }

        public int period {
            get {
                return _period;
            }
            set {
                _period = value;
            }
        }

        public DateTime start_date {
            get {
                return _start_date;
            }
            set {
                _start_date = value;
            }
        }

        public string Name {
            get {
                return _name;
            }

            set {
                _name = value;
            }
        }

        public string RA {
            get {
                return _RA;
            }

            set {
                _RA = value;
            }
        }

        public Aluno(String name, Curso course, int period, DateTime date) {
            reset_all();
            Name = name;
            _course = course;
            _period = period;
            _start_date = date;
            RA = set_RA(date);
            reset_grades((int)course);
            reset_freq((int)course);
        }
        #endregion
   
        #region Média, RA e notas
        public bool approved(int period, ref string result) {
            if (_period_freq[period] <= 75.0) {
                result = "Reprovado por frequência";
                return false;
            } else if (avg(period) <= 5.0) {
                result = "Reprovado";
                return false;
            } else {
                result = "Aprovado";
                return true;
            }
        }
        private double get_avg_freq() {
            var total_freq = 0.0;
            for (var x = 0; x < _period_freq.Count; x++) {
                total_freq += _period_freq[x];
            }
            return total_freq / _period_freq.Count;
        }
        private void reset_grades(int course) {

            if (course == 0) {
                _period_grade = new Dictionary<int, double[]>(_period + 1);
                for (var x = 0; x < _period; x++) {
                    _period_grade.Add(x, new double[3]);
                }
            } else if (course == 1) {
                _period_grade = new Dictionary<int, double[]>(_period + 1);
                for (var x = 0; x < _period; x++) {
                    _period_grade.Add(x, new double[3]);
                }
            } else if (course == 2) {
                _period_grade = new Dictionary<int, double[]>(_period + 1);
                for (var x = 0; x < _period; x++) {
                    _period_grade.Add(x, new double[2]);
                }
            } else {
                _period_grade = new Dictionary<int, double[]>(_period + 1);
                for (var x = 0; x < _period; x++) {
                    _period_grade.Add(x, new double[2]);
                }
            }
        }

        private void reset_freq(int course) {
            if (course == 0) {
                _period_freq = new Dictionary<int, double>(_period);
            } else if (course == 1) {
                _period_freq = new Dictionary<int, double>(_period);
            } else if (course == 2) {
                _period_freq = new Dictionary<int, double>(_period);
            } else {
                _period_freq = new Dictionary<int, double>(_period);
            }
        }
        public void set_grade(int period, double score, int index) {
            if (period <= _period) {
                if (score <= 10.0 && score >= 0.0) {
                    try {
                        _period_grade[period][index] = score;
                    } catch (Exception e) {
                        MessageBox.Show("Erro: período inválido.");
                    }
                } else {
                    throw new Exception("Nota inválida.");
                }
            } else {
                throw new Exception("Semestre inválido.");
            }
        }

        public void set_grade(int period, int score, int index) {
            if (_period <= period)
                if (score <= 10 && score >= 0)
                    if (!_period_grade.ContainsKey(period))
                        try {
                            _period_grade[period][index] = (Double)score;
                        } catch (Exception e) {
                            MessageBox.Show("Erro: período inválido.");
                        } else
                        throw new Exception("A nota desse período já foi cadastrada para esse aluno.");
                else
                    throw new Exception("Nota inválida.");
            else
                throw new Exception("Semestre inválido.");
        }
        public void set_freq(int period, double freq) {
            if (freq <= 100.0 && freq >= 0.0) {
                _period_freq[period] = freq;
            }
        }
        private string set_RA(DateTime d) {
            Random r = new Random();
            int n = r.Next(0, 150);
            var new_RA = "";
            do {
                new_RA = d.Year + n.ToString();
            } while (Alunos.exists_RA(new_RA));

            return new_RA;
        }
        public double get_freq(int days, int absences, double percent) {
            percent = (absences - days) / days;
            return percent;
        }
        public double avg() {
            var score_total = 0.0;
            var c = 0;
            for (var a = 0; a < _period_grade.Keys.Count; a++) {
                for (var b = 0; b < _period_grade[a].Length; b++) {
                    score_total += _period_grade[a][b];
                    c++;
                }
            }
            return score_total / c;
        }
        public double avg(int period) {
            var score_total = 0.0;
            var c = 0;
            for (var b = 0; b < _period_grade[period].Length; b++) {
                score_total += _period_grade[period][b];
                c++;
            }
            return score_total / c;
        }
        public string list_grades() {
            var grades = "Aluno: " + _name + "\n\n";
            for (var x = 0; x < _period_grade.Keys.Count; x++) {
                grades += ((int)_course > 1? "Semestre: " : "Ano: ") + (x + 1) +"\n";
                for(var b = 0; b < _period_grade[x].Length; b++) {
                    grades += "N" + (b+1) + ": " + _period_grade[x][b] + "\n";
                }
            }
            return grades;
        }
        #endregion
    }
}