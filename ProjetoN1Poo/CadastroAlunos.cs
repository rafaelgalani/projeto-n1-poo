﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoN1Poo {
    public partial class CadastroAlunos : Form {
        public int[] courses_periods = new int[] { 9, 3, 8, 10, 10, 10 };
        public string[] denom = new string[] { "º ano", "º semestre" };
        public int[] grades = new int[] { 3, 2 };
        public List<string> temp_grades;
        public List<string> temp_periods;

        public Aluno temp_aluno;

        public CadastroAlunos() {
            InitializeComponent();
            global_reset();
        }

        private void global_reset() {
            alunos_c.Text = Geral.Alunos.count().ToString();
            fill_courses();
            reset_components();
            groupBox2.Enabled = false;
        }

        private void reset_components() {
            curso_c.SelectedIndex = 0;
        }

        private void fill_courses() {
            curso_c.Items.Clear();
            foreach (var x in Enum.GetValues(typeof(Geral.Curso))) {
                curso_c.Items.Add(x.ToString().Replace('_', ' '));
            }
        }

        private void set_periods(int i) {
            periodo_c.Items.Clear();
            freq_c.Items.Clear();
            nota_c1.Items.Clear();
            nota_c2.Items.Clear();
            temp_grades = new List<string>();
            temp_periods = new List<string>();


            for (var x = 0; x < courses_periods[i]; x++) {
                periodo_c.Items.Add(i > 1 ? x + 1 + denom[1] : x + 1 + denom[0]);
                freq_c.Items.Add(i > 1 ? x + 1 + denom[1] : x + 1 + denom[0]);
                nota_c1.Items.Add(i > 1 ? x + 1 + denom[1] : x + 1 + denom[0]);
                temp_periods.Add(i > 1 ? x + 1 + denom[1] : x + 1 + denom[0]);
            }

            i = i > 1 ? 1 : 0;
            for (var x = 0; x < grades[i]; x++) {
                nota_c2.Items.Add("N" + (x + 1));
                temp_grades.Add("N" + (x + 1));
            }

            periodo_c.SelectedIndex = 0;
            freq_c.SelectedIndex = 0;
            nota_c1.SelectedIndex = 0;
            nota_c2.SelectedIndex = 0;
        }

        private void curso_c_SelectedIndexChanged(object sender, EventArgs e) {
            if (curso_c.SelectedIndex == -1) curso_c.SelectedIndex = 0;
            set_periods(curso_c.SelectedIndex);
        }

        private bool errors() {
            if (nome_c.Text.Trim() == "") {
                erro_c.SetError(nome_c, "Nome vazio.");
                return true;
            }
            return false;
        }

        private void add_student() {
            try {
                Geral.Alunos.add(temp_aluno);
            } catch (Exception e) {
                MessageBox.Show(e.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void change_status() {
            groupBox1.Enabled = !groupBox1.Enabled;
            groupBox2.Enabled = !groupBox2.Enabled;
        }

        private void remove_periods() {
            var c = periodo_c.Items.Count;
            for (var x = c - 1; x >= periodo_c.SelectedIndex + 1; x--) {
                freq_c.Items.RemoveAt(x);
                nota_c1.Items.RemoveAt(x);
            }
        }

        private void new_student_reset() {
            curso_c.SelectedIndex = -1;
            curso_c.SelectedIndex = 0;
            groupBox3.Enabled = true;
            groupBox4.Enabled = false;
        }

        private void prox_c_Click(object sender, EventArgs e) {
            if (!Geral.Alunos.exists_A(nome_c.Text))
                temp_aluno = new Aluno(nome_c.Text, (Geral.Curso)curso_c.SelectedIndex, periodo_c.SelectedIndex + 1, data_c.Value);
            else {
                MessageBox.Show("Já existe um aluno cadastrado com esse nome.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!errors()) {
                erro_c.Clear();
                change_status();
                remove_periods();
            }
        }

        private void cadastrar_c_Click(object sender, EventArgs e) {

        }

        private void nota_c2_Click(object sender, EventArgs e) {
            temp_aluno.set_grade(temp_periods.IndexOf(nota_c1.Items[nota_c1.SelectedIndex].ToString()), (Double)nota_v.Value, temp_grades.IndexOf(nota_c2.Items[nota_c2.SelectedIndex].ToString()));

            nota_c2.Items.RemoveAt(nota_c2.SelectedIndex);
            if (nota_c2.Items.Count > 0)
                nota_c2.SelectedIndex = 0;
            else {
                nota_c1.Items.RemoveAt(nota_c1.SelectedIndex);
                if (nota_c1.Items.Count > 0) {
                    nota_c1.SelectedIndex = 0;
                    for (var x = 0; x < temp_grades.Count; x++) {
                        nota_c2.Items.Add(temp_grades[x]);
                    }
                    nota_c2.SelectedIndex = 0;
                } else {
                    add_c.Enabled = true;
                    groupBox2.Enabled = false;
                }
            }
        }

        private void add_c_Click(object sender, EventArgs e) {
            add_student();
            groupBox2.Enabled = true;
            change_status();
            global_reset();
            new_student_reset();
            end_c.Enabled = true;
            add_c.Enabled = false;
        }

        private void end_c_Click(object sender, EventArgs e) {
            this.Hide();
            new ListagemAlunos().ShowDialog();
            Application.Exit();
        }

        private void freq_q_Click(object sender, EventArgs e) {
            temp_aluno.set_freq(freq_c.SelectedIndex, (Double)freq_v.Value);

            freq_c.Items.RemoveAt(freq_c.SelectedIndex);
            if (freq_c.Items.Count > 0) {
                freq_c.SelectedIndex = 0;
            } else {
                groupBox3.Enabled = false;
                groupBox4.Enabled = true;
            }
        }

        private void CadastroAlunos_Load(object sender, EventArgs e) {

        }
    }
}